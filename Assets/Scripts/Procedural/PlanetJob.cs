using System;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public struct PlanetJob : IJob, IDisposable
{
    public NativeArray<Vector3> Vertices;
    public NativeArray<int> Triangles;
    public NativeArray<Vector2> UV;

    ushort Width;
    ushort Height;
    float Radius;
    ushort Resolution;
    float Scale;

    public PlanetJob(ushort width, ushort height, float radius, ushort resolution, float scale)
    {
        Width = width;
        Height = height;
        Radius = radius;
        Resolution = resolution;
        Scale = scale;

        Vertices = new NativeArray<Vector3>(Resolution * Resolution, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
        Triangles = new NativeArray<int>((Resolution - 1) * (Resolution - 1) * 6, Allocator.TempJob, NativeArrayOptions.ClearMemory);
        UV = new NativeArray<Vector2>(Resolution * Resolution, Allocator.TempJob, NativeArrayOptions.ClearMemory);
    }

    public void Execute()
    {
        for (int y = 0; y < Resolution; y++)
        {
            for (int x = 0; x < Resolution; x++)
            {
                int index = (y * Resolution) + x;

                Vector3 vertex = new Vector3(-1f, -1f, -1f);

                if (y == 0 && x == 0)
                {
                    Vertices[index] = vertex;
                    continue;
                }

                vertex.x += (float)x / (Resolution - 1) * 2.0f;
                vertex.y -= (float)y / (Resolution - 1) * 2.0f;

                float x2 = vertex.x * vertex.x;
                float y2 = vertex.y * vertex.y;
                float z2 = vertex.z * vertex.z;

                vertex.x = vertex.x * Mathf.Sqrt(1.0f - (y2 * 0.5f) - (z2 * 0.5f) + ((y2 * z2) / 3.0f));
                vertex.y = vertex.y * Mathf.Sqrt(1.0f - (z2 * 0.5f) - (x2 * 0.5f) + ((z2 * x2) / 3.0f));
                vertex.z = vertex.z * Mathf.Sqrt(1.0f - (x2 - 0.5f) - (y2 - 0.5f) + ((x2 * y2) / 3.0f));

                vertex *= Radius;

                Vertices[x] = vertex;

                if (y < Resolution - 1 && x < Resolution - 1)
                {
                    var triIndex = 6 * index;
                    Triangles[triIndex++] = index;
                    Triangles[triIndex++] = index + Resolution;
                    Triangles[triIndex++] = index + Resolution + 1;

                    Triangles[triIndex++] = index;
                    Triangles[triIndex++] = index + Resolution + 1;
                    Triangles[triIndex++] = index + 1;
                }
            }
        }
    }

    public void Dispose()
    {
        Vertices.Dispose();
        Triangles.Dispose();
        UV.Dispose();
    }
}
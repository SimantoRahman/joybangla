using System;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEngine;

/// <summary>
/// A terrain manager which creates terrain chunks
/// manages them for optimization
/// and re-generates them when necessary
/// </summary>
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class TerrainManager : MonoBehaviour
{
    public bool useJob;
    [Header("Configuration")]
    public ushort Width;
    public ushort Height;
    public float Radius;
    public float Scale;
    public ushort Resolution;

    public Material MainMaterial;

    public List<Mask> Masks;

    // A list to refer to all created meshes
    List<GameObject> _chunks;
    PlanetJob _job;
    JobHandle _jobHandle;
    bool _isJobComplete;

    Mesh _planet;

    void Start()
    {
        // _chunks = new List<GameObject>();

        Initialize();

        if (useJob)
        {
            _job = new PlanetJob(Width, Height, Radius, Resolution, Scale);
            _jobHandle = _job.Schedule();
            _isJobComplete = false;
        }
    }

    void Update()
    {
        if (useJob && !_isJobComplete)
        {
            if (_jobHandle.IsCompleted)
            {
                _jobHandle.Complete();

                var meshRenderer = GetComponent<MeshRenderer>();
                var meshFilter = GetComponent<MeshFilter>();

                _planet.Clear();
                _planet.vertices = _job.Vertices.ToArray();
                _planet.triangles = _job.Triangles.ToArray();

                // _job.Vertices.CopyTo(_planet.vertices);
                // _job.Triangles.CopyTo(_planet.triangles);

                _planet.RecalculateBounds();
                _planet.RecalculateNormals();
                _planet.RecalculateTangents();

                _job.Dispose();
                _isJobComplete = true;

                meshFilter.sharedMesh = _planet;
                meshRenderer.sharedMaterial = MainMaterial;
            }
        }
        else if (!useJob && !_isJobComplete)
        {
            _planet = GenerateMesh();
            var meshRenderer = GetComponent<MeshRenderer>();
            var meshFilter = GetComponent<MeshFilter>();
            meshRenderer.sharedMaterial = MainMaterial;
            meshFilter.sharedMesh = _planet;
            _isJobComplete = true;
        }
    }

    Mesh GenerateMesh()
    {
        int triIndex = 0;
        Vector3[] vertices = new Vector3[Resolution * Resolution];
        int[] triangles = new int[(Resolution - 1) * (Resolution - 1) * 6];

        for (int y = 0; y < Resolution - 1; y++)
        {
            for (int x = 0; x < Resolution - 1; x++)
            {
                int index = (y * Resolution) + x;

                Vector3 vertex = new Vector3(-1f, -1f, -1f);

                vertex.x += (float)x / (Resolution - 1) * 2.0f;
                vertex.y -= (float)y / (Resolution - 1) * 2.0f;

                float x2 = vertex.x * vertex.x;
                float y2 = vertex.y * vertex.y;
                float z2 = vertex.z * vertex.z;

                vertex.x = vertex.x * Mathf.Sqrt(1.0f - (y2 * 0.5f) - (z2 * 0.5f) + ((y2 * z2) / 3.0f));
                vertex.y = vertex.y * Mathf.Sqrt(1.0f - (z2 * 0.5f) - (x2 * 0.5f) + ((z2 * x2) / 3.0f));
                vertex.z = vertex.z * Mathf.Sqrt(1.0f - (x2 - 0.5f) - (y2 - 0.5f) + ((x2 * y2) / 3.0f));

                vertex *= Radius;

                vertices[index] = vertex;

                if (y < Resolution - 1 && x < Resolution - 1)
                {
                    triangles[triIndex++] = index;
                    triangles[triIndex++] = index + Resolution;
                    triangles[triIndex++] = index + Resolution + 1;
                
                    triangles[triIndex++] = index;
                    triangles[triIndex++] = index + Resolution + 1;
                    triangles[triIndex++] = index + 1;
                }
            }
        }

        Mesh finalMesh = new Mesh() {
            vertices = vertices,
            triangles = triangles
        };

        finalMesh.RecalculateBounds();
        finalMesh.RecalculateNormals();
        finalMesh.RecalculateTangents();

        return finalMesh;
    }

    void Initialize()
    {
        _chunks = new List<GameObject>();
        _planet = new Mesh();
    }
}

public struct Mask
{
    public int Seed;
    public float Scale;
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Range(0.1f, 20f)]
    public float Speed;

    [Tooltip("Clamp angles in 3D space")]
    public Vector3 Clamp;

    private Vector3 direction, lookRotation;
    float horizontal, vertical, mouseX, mouseY;

    // Start is called before the first frame update
    void Start()
    {
        direction = new Vector3();
        lookRotation = new Vector3();
        StartCoroutine("Bug");

        InvokeRepeating(methodName: "Bug", 0f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        Look();
        Move();
    }

    void Move()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        direction.Set(horizontal, 0, vertical);
        direction.Normalize();

        transform.Translate(direction * Speed * Time.deltaTime * 5, Space.Self);
    }

    void Look()
    {
        mouseX = Input.GetAxis("Mouse X");
        mouseY = Input.GetAxis("Mouse Y");

        lookRotation.Set(-mouseY, mouseX, 0f);

        transform.Rotate(lookRotation, Space.Self);
    }

    void Bug()
    {
        Debug.Log($"Z rotation: {transform.rotation.z}");
        //Debug.Log($"Direction: {direction}");
    }
}
